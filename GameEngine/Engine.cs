﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    public class Engine
    {
        protected RenderWindow window;

        public bool IsWorks
        {
            get
            {
                return window.IsOpen;
            }
        }

        public Engine()
        {
            window = new RenderWindow(new VideoMode(640, 480), "GameEngine", Styles.Default);
            window.Closed += Window_Closed;
            window.Resized += Window_Resized;
        }

        private void Window_Resized(object sender, SizeEventArgs e)
        {
            window.SetView(new View(new FloatRect(0, 0, e.Width, e.Height)));
        }

        public void StartMainCycle()
        {
            Type obj = window.GetType();
            var arr = obj.GetMembers();
            foreach (var item in arr)
            {
                Console.WriteLine($"{item.Name}");
            }
            while (window.IsOpen)
            {
                window.DispatchEvents();

                window.Clear(Color.Black);
                window.Display();
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            window.Close();
        }
    }
}
