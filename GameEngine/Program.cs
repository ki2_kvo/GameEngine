﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Window;
using SFML.Graphics;

namespace GameEngine
{
    class Program
    {
        static Engine engine;

        static void Main(string[] args)
        {
            engine = new Engine();

            engine.StartMainCycle();
        }
    }
}
